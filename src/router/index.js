import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home.vue'
import Categoria from '../components/categorias/Categoria'
import Categoriaform from '../components/categorias/Categoriaform'
import Produto from '../components/produtos/Produto'
import Produtoform from '../components/produtos/Produtoform'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/categorias',
    name: 'categorias',
    component: Categoria
  },
  {
    path: '/categorias/novo',
    name: 'categorias-novo',
    component: Categoriaform
  },
  {
    path: '/categorias/editar/:id',
    name: 'categorias-editar',
    component: Categoriaform,
    props: true
  },
  {
    path: '/produtos',
    name: 'produtos',
    component: Produto
  },
  {
    path: '/produtos/novo',
    name: 'produtos-novo',
    component: Produtoform
  },
  {
    path: '/produtos/editar/:id',
    name: 'produtos-editar',
    component: Produtoform,
    props: true
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
