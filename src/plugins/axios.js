import Vue from 'vue'
import axios from 'axios'

axios.defaults.baseURL = 'http://desafiojoinapi.build.dev.br/api/v1'

Vue.use({
    install(Vue){
        Vue.prototype.$http = axios
    }
})